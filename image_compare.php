<?php

puzzle_set_noise_cutoff(.1);
$cvec1 = puzzle_fill_cvec_from_file('images/13.JPG');
$cvec2 = puzzle_fill_cvec_from_file('images/8b.JPG');

$d = puzzle_vector_normalized_distance( $cvec1, $cvec2 );

//echo $cvec1 . PHP_EOL;
//echo $cvec2 . PHP_EOL;
echo $d . PHP_EOL;
echo PUZZLE_CVEC_SIMILARITY_LOWER_THRESHOLD . PHP_EOL;

$compress_cvec1 = puzzle_compress_cvec($cvec1);
$compress_cvec2 = puzzle_compress_cvec($cvec2);

//echo $compress_cvec1 . PHP_EOL;
//echo $compress_cvec2 . PHP_EOL;