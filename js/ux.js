
//http://grimlock.local/nasa-globetrotters/api/getimages/id/1
//http://grimlock.local/nasa-globetrotters/api/getimages/lat/50
//http://grimlock.local/nasa-globetrotters/api/getimages/lng/-50
//http://grimlock.local/nasa-globetrotters/api/getimages/geon/congo
//http://grimlock.local/nasa-globetrotters/api/getimages/feat/congo
//http://grimlock.local/nasa-globetrotters/api/getimages/limit/10/random
//http://grimlock.local/nasa-globetrotters/api/getimages/lat/32.5/lng/-65/geon/bermuda/feat/bermuda/limit/10/random



	var baseImageUri = "http://grimlock.local/nasa-globetrotters/api/getimages/",
		limit = 8,
		imgArr = [],
		loadFirst = true,
		loaderImg = "img/spaceship.gif";

	function LoadImages(random){

		var uri = baseImageUri + "limit/" + limit;

		if(random){

			uri += "/random";
		}

		Ajax(uri, RenderImages, "", "GET");
	
	}

	function RenderImages(data){

		var renderDiv = $('#imagesContainer'),
			images = '';

		$.each(data, function() {

			imgArr.push(this);
			images += "<img src='" + this.url + "' id='" + this.id + "' onclick='AddToGmaps(\"" + this.url + "\",\"" + this.id + "\")' />";
		});
		
		renderDiv.html(images);

		if(loadFirst){
			
			var fabricImg = $('#my-img');
			fabricImg.attr("src", imgArr[0].url);
			fabricImg.attr("rel", imgArr[0].id);
			console.log(imgArr[0]);

			InitFabric();
			loadFirst = false;

		}
	}
	
	function AddToGmaps(imgUrl, imgId){

		var fabricImg = $('#my-img');
		fabricImg.attr("src", imgUrl);
		fabricImg.attr("rel", imgId);


		var activeObject = canvas.getElement();
		var src = activeObject.src;
		var newsrc = imgUrl;
		activeObject.setAttribute("src", newsrc);
		canvas.renderAll();


	}
	
	function ShowLoader(){

		$("#msg").html("<img src='" + loaderImg + "'/>");
		

	}

	function RemoveLoader(){
		$('#msg').html('');
	}

	function Ajax(url, returnFunction, jsonData, type){

		ShowLoader();

		$.ajax({
			type: type,
			data: jsonData,
			url: url,
			success: function(data){
			  console.log(data);
			  RemoveLoader();
			  returnFunction(data);
			},
			dataType: 'json'
		  });
	}
