var map, mapOptions, canvas, imgElement, imgInstance, context,
    compareUri = "http://grimlock.local/nasa-globetrotters/api/compare";


function getPixelCount( $dimensions ) {
	return $dimensions[0] * $dimensions[1];
}

function InitGmaps() {

  mapOptions = {
    center: new google.maps.LatLng(16.5, -14.5),
    zoom: 3,
    mapTypeId: google.maps.MapTypeId.SATELLITE
  };
  map = new google.maps.Map(document.getElementById("map-canvas"),
      mapOptions);
}

function GetGmapsImageUrl(width, height){

  //http://maps.googleapis.com/maps/api/staticmap?center=40.714728,-73.998672&zoom=12&size=400x400&sensor=false

  var gmapUri = "http://maps.googleapis.com/maps/api/staticmap?center=",
    zoom = map.getZoom(),
    center = map.getCenter(),
    imageUrl = '';
    
  var lat = center.lat();
  var lng = center.lng();

  size = Math.floor(width) + "x" + Math.floor(height);

  imageUrl = gmapUri + lat + "," + lng + "&zoom=" + zoom + "&size=" + size + "&sensor=false&maptype=satellite";

  return imageUrl;
  
}

function TakeSnapshot(){
  
  var imageHeight = imgInstance.currentHeight,
      imageWidth = imgInstance.currentWidth,
      imageAngle = imgInstance.angle;


	var bounds = imgInstance.getBoundingRect();
	console.log(imageAngle);
	console.log(imageWidth);
	console.log(imageHeight);
	console.log(canvas);

	var angleInRadians = imageAngle * Math.PI / 180;

  var crop = GetCropCoordinates(angleInRadians, {w: imageWidth, h:imageHeight});


  context = canvas.getContext('2d');
	console.log(bounds);
	console.log(crop);

	imgInstance.setOpacity(1);
	canvas.renderAll();

	var modifier = 10;
	var x = bounds.left + crop.x + modifier;
	var y = bounds.top + crop.y + modifier;
	var width = crop.w - (modifier * 2);
	var height = crop.h - (modifier * 2);
    var fabricImage = context.getImageData(x, y, width, height);
	console.log(fabricImage);


	var tmp = document.getElementById('temp-canvas');
	var tmpCtx = tmp.getContext('2d');

	tmp.width = fabricImage.width;
	tmp.height = fabricImage.height;
	tmpCtx.putImageData(fabricImage, 0, 0);

  var fabricData = tmp.toDataURL();

	var gMapsImageUrl = GetGmapsImageUrl(fabricImage.width, fabricImage.height);

	var c = document.getElementById('c');
	var cctx = c.getContext('2d');
//	cctx.fillStyle = 'red';
	cctx.strokeRect(x, y, fabricImage.width, fabricImage.height);

	//console.log(fabricData);
  //console.log("gMapsImageUrl", gMapsImageUrl);
  //console.log("fabricImage", fabricImage);

  var compareData = {'gMaps': gMapsImageUrl, 'fabric': fabricData };

  Ajax(compareUri, DisplayScore, compareData, "POST");


}

function DisplayScore(data){

  var percent = "<span>Match Likelihood:" + Math.floor(100 - data.matchPercentage * 100) + " percent</span><br/>";
  var GmapImage = "<img src='" + data.gmapUrl + "'/>";
  var fabricImage = "<img src='" + data.fabricUrl + "'/>";
	var fuzzImage = "<img class='fuzz' src='" + data.fuzzUrl + "'/>";
  
  var strHtml = percent + GmapImage + fabricImage + fuzzImage;
  
  $("#match").html(strHtml);

}

function InitFabric(){

  canvas = new fabric.Canvas('c');
  imgElement = document.getElementById('my-img');
  
  imgInstance = new fabric.Image(imgElement, {
    left: 200,
    top: 200,
    width:200,
    height:200,
    angle: 30,
    opacity: 0.50
  });
  
  canvas.add(imgInstance);
  canvas.item(0).lockMovementX = true;
  canvas.item(0).lockMovementY = true;
	canvas.item(0).hasControls = false;

	context = canvas.getContext('2d');

	var angleControl = document.getElementById('angle-control');
	angleControl.onchange = function() {
		imgInstance.setAngle(this.value).setCoords();
		canvas.renderAll();
	};

	var scaleControl = document.getElementById('scale-control');
	scaleControl.onchange = function() {
		imgInstance.scale(this.value).setCoords();
		canvas.renderAll();
	};

	var opacityControl = document.getElementById('opacity-control');
	opacityControl.onchange = function() {
		imgInstance.setOpacity(this.value).setCoords();
		canvas.renderAll();
	};
}

function GetCropCoordinates(angleInRadians, imageDimensions) {

	var ang = angleInRadians;
	var img = imageDimensions;

	var quadrant = Math.floor(ang / (Math.PI / 2)) & 3;
	var sign_alpha = (quadrant & 1) === 0 ? ang : Math.PI - ang;
	var alpha = (sign_alpha % Math.PI + Math.PI) % Math.PI;

	var bb = {
		w: img.w * Math.cos(alpha) + img.h * Math.sin(alpha),
		h: img.w * Math.sin(alpha) + img.h * Math.cos(alpha)
	};

	var gamma = img.w < img.h ? Math.atan2(bb.w, bb.h) : Math.atan2(bb.h, bb.w);

	var delta = Math.PI - alpha - gamma;

	var length = img.w < img.h ? img.h : img.w;
	var d = length * Math.cos(alpha);
	var a = d * Math.sin(alpha) / Math.sin(delta);

	var y = a * Math.cos(gamma);
	var x = y * Math.tan(gamma);

	return {
		x: x,
		y: y,
		w: bb.w - 2 * x,
		h: bb.h - 2 * y
	};
}

function Init(){
  InitGmaps();
  LoadImages();

}
