/***************************/
//@Author: Adrian "yEnS" Mato Gondelle
//@website: www.yensdesign.com
//@email: yensamg@gmail.com
//@license: Feel free to use it, but keep this credits please!					
/***************************/
//Foo1 Page Pop Up
			var popupFoo1Status = 0;
			
			function loadPopupFoo1(){
				if(popupFoo1Status==0){
					$("#popupFoo1").fadeIn("slow");
					popupFoo1Status = 1;
				}
			}
			
			function disablePopupFoo1(){
				if(popupFoo1Status==1){
					$("#popupFoo1").fadeOut("slow");
					popupFoo1Status = 0;
				}
			}
			
			function centerPopupFoo1(){
				var windowWidth = document.documentElement.clientWidth;
				var windowHeight = document.documentElement.clientHeight;
				var popupFoo1Height = $("#popupFoo1").height();
				var popupFoo1Width = $("#popupFoo1").width();
				$("#popupFoo1").css({
					"position": "absolute",
					"top": windowHeight/2-popupFoo1Height/2,
					"left": windowWidth/2-popupFoo1Width/2
				});
			}
			
			
			$(document).ready(function(){
				$("#popupFoo1").fadeOut();
				popupFoo1Status = 0;
				$("#Foo1").click(function(){
				$("#popupFoo1").css({
					"visibility": "visible"	});
					disablePopupFoo2();
					disablePopupFoo3();
					centerPopupFoo1();
					loadPopupFoo1();
				});
				$("#popupFoo1Close").click(function(){
					disablePopupFoo1();
				});
			});
			$(function()
			{
				$('#popupFoo1').jScrollPane();
				$('.popupFoo1').jScrollPane(
					{
						showArrows: true,
						horizontalGutter: 10
					}
				);
			});
//Foo2 Page Pop Up
			var popupFoo2Status = 0;
			
			function loadPopupFoo2(){
				if(popupFoo2Status==0){
					$("#popupFoo2").fadeIn("slow");
					popupFoo2Status = 1;
				}
			}
			
			function disablePopupFoo2(){
				if(popupFoo2Status==1){
					$("#popupFoo2").fadeOut("slow");
					popupFoo2Status = 0;
				}
			}
			
			function centerPopupFoo2(){
				var windowWidth = document.documentElement.clientWidth;
				var windowHeight = document.documentElement.clientHeight;
				var popupFoo2Height = $("#popupFoo2").height();
				var popupFoo2Width = $("#popupFoo2").width();
				$("#popupFoo2").css({
					"position": "absolute",
					"top": windowHeight/2-popupFoo2Height/2,
					"left": windowWidth/2-popupFoo2Width/2
				});
			}
			
			
			$(document).ready(function(){
				$("#popupFoo2").fadeOut();
				popupFoo2Status = 0;
				$("#Foo2").click(function(){
				$("#popupFoo2").css({
					"visibility": "visible"	});
					disablePopupFoo1();
					disablePopupFoo3();					
					centerPopupFoo2();
					loadPopupFoo2();
				});
				$("#popupFoo2Close").click(function(){
					disablePopupFoo2();
				});
			});
			$(function()
			{
				$('#popupFoo2').jScrollPane();
				$('.popupFoo2').jScrollPane(
					{
						showArrows: true,
						horizontalGutter: 10
					}
				);
			});
//Foo3 Page Pop Up
			var popupFoo3Status = 0;
			
			function loadPopupFoo3(){
				if(popupFoo3Status==0){
					$("#popupFoo3").fadeIn("slow");
					popupFoo3Status = 1;
				}
			}
			
			function disablePopupFoo3(){
				if(popupFoo3Status==1){
					$("#popupFoo3").fadeOut("slow");
					popupFoo3Status = 0;
				}
			}
			
			function centerPopupFoo3(){
				var windowWidth = document.documentElement.clientWidth;
				var windowHeight = document.documentElement.clientHeight;
				var popupFoo3Height = $("#popupFoo3").height();
				var popupFoo3Width = $("#popupFoo3").width();
				$("#popupFoo3").css({
					"position": "absolute",
					"top": windowHeight/2-popupFoo3Height/2,
					"left": windowWidth/2-popupFoo3Width/2
				});
			}
			
			
			$(document).ready(function(){
				$("#popupFoo3").fadeOut();
				popupFoo3Status = 0;
				$("#Foo3").click(function(){
				$("#popupFoo3").css({
					"visibility": "visible"	});
					disablePopupFoo1();
					disablePopupFoo2();					
					centerPopupFoo3();
					loadPopupFoo3();
				});
				$("#popupFoo3Close").click(function(){
					disablePopupFoo3();
				});
			});
			$(function()
			{
				$('#popupFoo3').jScrollPane();
				$('.popupFoo3').jScrollPane(
					{
						showArrows: true,
						horizontalGutter: 10
					}
				);
			});
