<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    
    <style type="text/css">
      html { height: 100% }
      body { height: 100%; margin: 0; padding: 0 }
      #map-canvas {
        width: 800px;
        height:550px;
        z-index:10;
        position:absolute;
        left:0px;
        top:0px;
      }
      
      #image-canvas {
        position: absolute;
        top: 30;
        left: 70;
        z-index:20;
      }

    </style>

   
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA4vsncpVhDtWLmDSLyIHkOeulBfFMWVzE&sensor=true"></script>
    <script type="text/javascript" src="js/core.js"></script>
    <script type="text/javascript" src="js/fabric.js"></script>
    
  </head>
  <body onload="Init()">

    <div style="position: relative; left: 0; top: 0;">

      <div id="map-canvas"></div>

      <div id="image-canvas" width="500" height="500">
        <canvas id="c" width="500" height="500" style="border:1px solid #aaa"></canvas>
        <img src="images/6.jpg" id="my-img" style="display:none;">
      </div>
      
    </div>

    <div style="float:right;height:500px;width:500px;border:1px solid #aaa">
      <input type="button" value="GetMapSnapshot" onclick="TakeSnapshot()"/>
    </div>






  </body>
</html>