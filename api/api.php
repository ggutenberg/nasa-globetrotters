<?php
    
	/* 
		This is an example class script proceeding secured API
		To use this class you should keep same as query string and function name
		Ex: If the query string value rquest=delete_user Access modifiers doesn't matter but function should be
		     function delete_user(){
				 You code goes here
			 }
		Class will execute the function dynamically;
		
		usage :
		
		    $object->response(output_data, status_code);
			$object->_request	- to get santinized input 	
			
			output_data : JSON (I am using)
			status_code : Send status message for headers
			
		Add This extension for localhost checking :
			Chrome Extension : Advanced REST client Application
			URL : https://chrome.google.com/webstore/detail/hgmloofddffdnphfgcellkdfbfbjeloo
		
		I used the below table for demo purpose.
		
		CREATE TABLE IF NOT EXISTS `users` (
		  `user_id` int(11) NOT NULL AUTO_INCREMENT,
		  `user_fullname` varchar(25) NOT NULL,
		  `user_email` varchar(50) NOT NULL,
		  `user_password` varchar(50) NOT NULL,
		  `user_status` tinyint(1) NOT NULL DEFAULT '0',
		  PRIMARY KEY (`user_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
 	*/
	
	require_once("Rest.inc.php");
	
	class API extends REST {
	
		public $data = "";
		
		const DB_SERVER = "localhost";
		const DB_USER = "nasa";
		const DB_PASSWORD = "nasa";
		const DB = "nasa";
		
		private $db = NULL;
	
		public function __construct(){
			parent::__construct();				// Init parent contructor
			$this->dbConnect();					// Initiate Database connection
		}
		
		/*
		 *  Database connection 
		*/
		private function dbConnect(){
			$this->db = new mysqli(self::DB_SERVER,self::DB_USER,self::DB_PASSWORD, self::DB);
		}
		
		/*
		 * Public method for access api.
		 * This method dynamically call the method based on the query string
		 *
		 */
		public function processApi(){
			$request = explode( '/', $_REQUEST['rquest'] );
			$func = array_shift( $request );

			if( (int) method_exists( $this, $func ) > 0 )
				//call_user_func_array( $this->$func, $request );
				$this->$func( $request );
			else
				$this->response( '', 404 );	// If the method not exist with in this class, response would be "Page not found".
		}

		private function compare( $args ) {
			putenv('PATH=/opt/local/bin');

//			$image_data = getimagesize( 'gmap.png' );
//			$pixel_count = $image_data[0] * $image_data[1];
//			$output = shell_exec( 'compare -metric AE -fuzz 15% fabric.png gmap.png null: 2>&1' );
//			echo $output . '/' . $pixel_count;

			if ( $this->get_request_method() != 'POST' ) {
				$this->response( '', 406 );
			}

			//error_log(json_encode($_POST));

			$gmap = file_get_contents( $_POST['gMaps'] );
			file_put_contents( '../images/gmap.png', $gmap );

			$fabric = $_POST['fabric'];
			$img = str_replace( 'data:image/png;base64,', '', $fabric );
			$img = str_replace( ' ', '+', $img );
			$data = base64_decode( $img );
			file_put_contents( '../images/fabric.png', $data);

			$image_data = getimagesize( '../images/gmap.png' );
			$pixel_count = $image_data[0] * $image_data[1];

			chdir('../images');
			//$output = shell_exec( 'compare -metric AE -fuzz 15% fabric.png gmap.png null: 2>&1' );
			$output = shell_exec( 'compare -metric AE -fuzz 15% fabric.png gmap.png fuzz.png 2>&1' );
			$compare_response = array(
				'matchPercentage' => $output / $pixel_count,
				'gmapUrl' => 'http://grimlock.local/nasa-globetrotters/images/gmap.png',
				'fabricUrl' => 'http://grimlock.local/nasa-globetrotters/images/fabric.png',
				'fuzzUrl' => 'http://grimlock.local/nasa-globetrotters/images/fuzz.png'
			);
			$this->response( $this->json($compare_response), 200 );
			//echo $output . '/' . $pixel_count;

		}

		private function getimages( $args ) {
			if ( $this->get_request_method() != 'GET' ) {
				$this->response( '', 406);
			}

			//error_log(print_r($_SERVER, true));

			$sql = 'SELECT * FROM images';
			$where = array();
			$order = '';
			$limit = '';

			if ( false !== ( $key = array_search( 'id', $args ) ) ) {
				if ( 'null' == $args[$key + 1] ) {
					$where[] = 'id IS NULL';
				}
				else {
					$where[] = 'id=' . $args[$key + 1];
				}
			}

			if ( false !== ( $key = array_search( 'lat', $args ) ) ) {
				if ( 'null' == $args[$key + 1] ) {
					$where[] = 'lat IS NULL';
				}
				else {
					$where[] = 'lat=' . $args[$key + 1];
				}
			}

			if ( false !== ( $key = array_search( 'lng', $args ) ) ) {
				if ( 'null' == $args[$key + 1] ) {
					$where[] = 'lng IS NULL';
				}
				else {
					$where[] = 'lng=' . $args[$key + 1];
				}
			}

			if ( false !== ( $key = array_search( 'geon', $args ) ) ) {
				if ( 'null' == $args[$key + 1] ) {
					$where[] = 'geon LIKE \'\'';
				}
				else {
					$where[] = 'geon LIKE \'%' . strtoupper( html_entity_decode( $args[$key + 1] ) ) . '%\'';
				}
			}

			if ( false !== ( $key = array_search( 'feat', $args ) ) ) {
				if ( 'null' == $args[$key + 1] ) {
					$where[] = 'feat LIKE \'\'';
				}
				else {
					$where[] = 'feat LIKE \'%' . strtoupper( html_entity_decode( $args[$key + 1] ) ) . '%\'';
				}
			}

			if ( false !== ( $key = array_search( 'random', $args ) ) ) {
				$order = ' ORDER BY RAND() ';
			}

			if ( false !== ( $key = array_search( 'limit', $args ) ) ) {
				$limit = $args[$key + 1];
			}

			if ( 0 < count($where) ) {
				$sql .= ' WHERE ' . $where[0];
				if ( 1 < count($where) ) {
					for ( $i = 1; $i < count($where); $i++ ) {
						$sql .= ' AND ' . $where[$i];
					}
				}
			}

			if ( '' != $order ) {
				$sql .= $order;
			}

			if ( '' != $limit ) {
				$sql .= ' LIMIT ' . $limit;
			}

			$result = $this->db->query( $sql );
			if ( 0 < $result->num_rows ) {
				$set = array();
				while ( $row = $result->fetch_array( MYSQLI_ASSOC ) ) {
					if ( ! file_exists( '../images/' . $row['id'] . '.jpg' ) ) {
						$file = file_get_contents( $row['url'] );
						file_put_contents( '../images/' . $row['id'] . '.jpg', $file );
					}
					$row['url'] = 'http://grimlock.local/nasa-globetrotters/images/' . $row['id'] . '.jpg';
					$set[] = $row;
				}
				$this->response($this->json( $set ), 200);
				$result->close();
			}

			$this->response( '', 204 );

			$this->db->close();

		}


		/*
		 *	Encode array into JSON
		*/
		private function json($data){
			if(is_array($data)){
				return json_encode($data);
			}
		}
	}
	
	// Initiate Library
	
	$api = new API;
	$api->processApi();
?>