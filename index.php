<!DOCTYPE html>
<html>
  <head>
    <title>Bootstrap 101 Template</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">

      <script src="http://code.jquery.com/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="http://www.google.com/jsapi?key=ABQIAAAAwbkbZLyhsmTCWXbTcjbgbRSzHs7K5SvaUdm8ua-Xxy_-2dYwMxQMhnagaawTo7L1FE1-amhuQxIlXw"></script>

     <script>
      
        google.load("earth", "1");

        var ge = null;

        function init() {
          google.earth.createInstance("map3d", initCallback, failureCallback);
        }

        function initCallback(object) {
          ge = object;
          ge.getWindow().setVisibility(true);
          ge.getLayerRoot().enableLayerById(ge.LAYER_BORDERS, true);
        }

        function failureCallback(object) {
          alert("GE Init failed!!!!!!!!!!!!!!!!!!!")
        }

        //The google namespace includes the setOnLoadCallback() function, which calls the specified function once the HTML page and requested APIs have been loaded. Using this function ensures that the plugin is not loaded until the page's DOM is completely built out.
        //google.setOnLoadCallback(init);

        //google.earth.addSideDatabase(
        //  ge, 'http://khmdb.google.com/?db=mars',
        //    sideDatabaseSuccess,
        //    sideDatabaseFail,
        //    {
        //      userName: '',
        //      password: ''
        //    }
        //);

        
    </script>

  </head>
  <body onload='init()' id='body'>
    <h1>Hello, Earth!</h1>

    <div id='map3d' style='border: 1px solid silver; height: 600px; width: 800px;'></div>
  </body>
</html>