<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    
    
    <link href="css/global.css" rel="stylesheet" media="screen">

    <script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA4vsncpVhDtWLmDSLyIHkOeulBfFMWVzE&sensor=true"></script>
	  <script type="text/javascript" src="js/fabric.js"></script>

  </head>
  
  <body onload="Init()">

    <div id=container">

      <div id="map-canvas"></div>

      <div id="image-canvas">
        <canvas id="c" width="400" height="400"></canvas>
        <img src="" id="my-img">
      </div>
      
    </div>

    <div id="controls" >

      <input type="button" value="Compare Regions" onclick="TakeSnapshot()"/>
      <input type="button" value="Get Images" onclick="LoadImages()"/>
      <input type="button" value="Get More Images" onclick="LoadImages(true)"/>
	    <p>
		    <label><span>Angle:</span> <input type="range" id="angle-control" value="30" min="-180" max="180"></label>
	    </p>
	    <p>
		    <label><span>Scale:</span> <input type="range" id="scale-control" value="1" min="0.1" max="3" step="0.1"></label>
	    </p>
	    <p>
		    <label><span>Opacity:</span> <input type="range" id="opacity-control" value="0.5" min="0" max="1" step="0.1"></label>
	    </p>

        <p>
		    <label><div id="match"></div></label>
	    </p>

        <p><div id="msg"></div></p>
    </div>

	<canvas id="temp-canvas" style="display:none;"></canvas>

    <div id="imagesContainer"></div>



    <script type="text/javascript" src="js/core.js"></script>
    <script type="text/javascript" src="js/ux.js"></script>

  </body>
</html>